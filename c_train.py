# you MUST use it, if you making convesational model
# NOT TESTED
import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras import layers, Model
import json

# Load existing model and tokenizer
model = tf.keras.models.load_model("./pretrained/model")
tokenizer_json = json.loads(open("./pretrained/tokenizer.json", "r", encoding="utf-8").read())
tokenizer = tokenizer_from_json(tokenizer_json)

# Load new dataset
with open("new_dataset.txt", "r", encoding="utf-8") as file:
    data = file.read()

# Prepare new dataset
new_sentences = data.split("\n")
new_input_sequences = []
for line in new_sentences:
    token_list = tokenizer.texts_to_sequences([line])[0]
    for i in range(1, len(token_list)):
        n_gram_sequence = token_list[:i + 1]
        new_input_sequences.append(n_gram_sequence)

# Pad new sequences
new_max_sequence_len = max([len(seq) for seq in new_input_sequences])
new_input_sequences = pad_sequences(new_input_sequences, maxlen=new_max_sequence_len, padding="pre")

# Create new data
new_xs = new_input_sequences[:, :-1]
new_labels = new_input_sequences[:, -1]
new_ys = tf.keras.utils.to_categorical(new_labels, num_classes=len(tokenizer.word_index) + 1)

# Continue training
print("Continuing training...")
model.fit(new_xs, new_ys, epochs=epochs, batch_size=batch_size, callbacks=[tensorboard_callback])

# Save new model
model.save("./ready_model/model")

# Save new tokenizer
tokenizer_json = tokenizer.to_json()
with open("./ready_model/tokenizer.json", "w", encoding="utf-8") as f:
    f.write(json.dumps(tokenizer_json, ensure_ascii=False))

# Save max_sequence_len
with open("./ready_model/max_sequence_len.txt", "w", encoding="utf-8") as f:
    f.write(str(new_max_sequence_len))
