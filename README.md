# textgen
Generate text with LSTM/Transformer model

# Downloading pretrained model
## textgen-lstm-70k-36iter
Model based on LSTM, trained on 70 000 lines of text with 36 iterations

``git clone https://codeberg.org/DFCTeam/textgen-lstm-70k-36iter``
Rename folder to ``pretrained``

Start ``run.py`` and enjoy

# Before running train.py, c_train.py or run.py
Create ``pretrained`` and ``ready_model`` folders

For c_train.py - Create ``new_dataset.txt``

# Scripts
``train.py`` - Train model

``c_train.py`` - Continue/idk train. Old model must be saved to pretrained. You MUST use it, if you making convesational model

``run.py`` - Run pretrained model